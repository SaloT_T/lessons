function addContact(obj) {
    $('div.phone > ul.phonebook').append('<li class="list-group-item" data-id="' + obj.id + '">' + obj.name + '<span class="icons"><i class="fa fa-envelope" id="msg" aria-hidden="true" data-toggle="modal" data-target="#exampleModal"></i><i class="fa fa-times" id="removeCont" aria-hidden="true"></i></span>');
}

var contactList = localStorage;

for (var contact in contactList) {
    var item = JSON.parse(contactList[contact]);
    addContact(item);
}
///////Сохранение контакта//////////////////
$('.saveContact').click(function () {
    var currentId = $(this).attr('data-id');
    var id = 'id' + localStorage.length + 1;

    console.log(currentId, typeof currentId);
    if (currentId !== undefined) {
        localStorage.removeItem(currentId);
        $('li[data-id=' + currentId + ']').remove();
        id = currentId;

    }

    var contact = {
        id: id,
        name: $('#contName').val(),
        email: $('#email').val(),
        tel: $('#tel').val()
    };

    addContact(contact);

    localStorage.setItem(id, JSON.stringify(contact));

    $('#contName').val('');
    $('#email').val('');
    $('#tel').val('');

    $('#myModal').modal('hide');
});
///////вызов окна для отправки сообщения///////////////////
$(document).on('click', '#msg', function () {
    var currentId = $(this).parent().parent().attr('data-id');
    var item = JSON.parse(localStorage.getItem(currentId));
    console.log(item);
    $('#exampleModal #name').val(item.name);
    $('#exampleModal #tel').val(item.tel);
    $('#msgText').val('');
});
///////Удаление контакта////////////////////
$(document).on('click', '#removeCont', function () {
    var itemId = $(this).parent().parent().attr('data-id');
    if (confirm("Вы действительно хотите удалить?")) {
        localStorage.removeItem(itemId);
        $(this).parent().parent().remove();
    }
});
///////Кнопка добавить контакт//////////////
$(".addContact").click(function () {
    $('#contName').val('');
    $('#email').val('');
    $('#tel').val('');
});

///////Отправка смс////////////////////////
$('#sendMsg').click(function () {
    var text = $('#msgText').val();
    var tel = $('#exampleModal #tel').val();
    console.log(tel)
    console.log(text)
    $.get("https://api.mobizon.com/service/message/sendsmsmessage?apiKey=f4ff11d7cb30675ea112f19f51beb977efcc1461&recipient=38" + tel + "&text=" + text);
    $('#exampleModal').modal('hide');
});
