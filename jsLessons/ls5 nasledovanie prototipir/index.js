//var car = {
//    color: "white",
//    engine: "V8",
//    wheels: 4
//};
//
//var audi = {
//    brand: "Audi",
//    model: "TT",
//    color: "black"
//};
//
//audi.__proto__ = car;
//
//console.log(audi.color);
//delete audi.color;
//console.log(audi.color);
//
/////////////////////////////////////
//
//function Audi(){
//    this.color = 'black';
//    this.model = 'TT';
//    this.brand = "Audi";
//}
//function Car(color){
//    this.color = color;
//    this.wheels = 4;
//    this.engine = 'V8';
//}
//
//var car = new Car('black');
//Audi.prototype = car;
//var audi = new Audi();
//console.log(audi.engine);
//console.log(audi.engine);

//var str = "Hello world";
//console.log(str.indexOf('l'));
//console.log(str.length);
//////////////////////////////////////
//String.prototype.addPrefix = function(p) {
//    return p + this;
//};
//var str = "Hello world";
//console.log(str.addPrefix("ex_"));
///////////////////////////////////////

var a = {
    color: 'white',
    size: 340
};

var b = Object.assign(a);
console.log(b.color);
b.color = 'black';
console.log(b.color);
console.log(a.color);
a.color = 'red';
console.log(b.color);
/////////////////////////
