-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.53 - MySQL Community Server (GPL)
-- Операционная система:         Win32
-- HeidiSQL Версия:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных testingBD
CREATE DATABASE IF NOT EXISTS `testingbd` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `testingBD`;

-- Дамп структуры для таблица testingBD.brands
CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы testingBD.brands: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` (`id`, `name`, `description`) VALUES
	(20, 'Черниговское', 'тест'),
	(21, 'Львовское', 'тест'),
	(22, 'Моршинская', 'тест'),
	(23, 'Тархун', 'тест');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;

-- Дамп структуры для таблица testingBD.category
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы testingBD.category: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`id`, `category`) VALUES
	(16, 'Пиво'),
	(17, 'Вода'),
	(18, 'Лимонад');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

-- Дамп структуры для таблица testingBD.cities
CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(50) NOT NULL,
  `region_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `city_region` (`region_id`),
  CONSTRAINT `city_region` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы testingBD.cities: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` (`id`, `city`, `region_id`) VALUES
	(6, 'Днепр', 5),
	(7, 'Бердянск', 7),
	(8, 'Киев', 6);
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;

-- Дамп структуры для таблица testingBD.countryes
CREATE TABLE IF NOT EXISTS `countryes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы testingBD.countryes: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `countryes` DISABLE KEYS */;
INSERT INTO `countryes` (`id`, `country`) VALUES
	(2, 'Украина');
/*!40000 ALTER TABLE `countryes` ENABLE KEYS */;

-- Дамп структуры для таблица testingBD.delivery
CREATE TABLE IF NOT EXISTS `delivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы testingBD.delivery: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `delivery` DISABLE KEYS */;
INSERT INTO `delivery` (`id`, `delivery`) VALUES
	(4, 'Курьером'),
	(5, 'До склада'),
	(6, 'Самовывоз');
/*!40000 ALTER TABLE `delivery` ENABLE KEYS */;

-- Дамп структуры для таблица testingBD.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `delivery_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `order_user` (`user_id`),
  KEY `order_product` (`product_id`),
  KEY `order_payment` (`payment_id`),
  KEY `order_delivery` (`delivery_id`),
  KEY `order_status` (`status_id`),
  CONSTRAINT `order_delivery` FOREIGN KEY (`delivery_id`) REFERENCES `delivery` (`id`),
  CONSTRAINT `order_payment` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`),
  CONSTRAINT `order_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `order_status` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`),
  CONSTRAINT `order_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы testingBD.orders: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` (`id`, `user_id`, `product_id`, `payment_id`, `delivery_id`, `status_id`, `description`, `date`) VALUES
	(1, 27, 21, 4, 6, 1, 'перезвоните мне', '2017-07-21 12:42:38'),
	(3, 27, 22, 4, 6, 3, 'отмена', '2017-07-21 12:43:33'),
	(4, 24, 18, 3, 4, 2, 'жду заказ', '2017-07-21 12:44:11'),
	(5, 25, 20, 3, 5, 2, 'в ожидании', '2017-07-21 12:44:39');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Дамп структуры для таблица testingBD.payments
CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы testingBD.payments: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` (`id`, `payment`) VALUES
	(3, 'Наличные'),
	(4, 'Privat24');
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;

-- Дамп структуры для таблица testingBD.product
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `volume_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_category` (`category_id`),
  KEY `product_brand` (`brand_id`),
  KEY `product_volume` (`volume_id`),
  CONSTRAINT `product_brand` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`),
  CONSTRAINT `product_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  CONSTRAINT `product_volume` FOREIGN KEY (`volume_id`) REFERENCES `volume` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы testingBD.product: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`id`, `brand_id`, `category_id`, `volume_id`, `price`) VALUES
	(18, 21, 16, 13, 25.00),
	(20, 20, 16, 11, 15.35),
	(21, 23, 18, 12, 22.00),
	(22, 22, 17, 13, 13.00);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Дамп структуры для таблица testingBD.profile
CREATE TABLE IF NOT EXISTS `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `age` varchar(50) NOT NULL,
  `tel` varchar(50) NOT NULL,
  `city_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `gender` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `profile_user` (`user_id`),
  KEY `profile_city` (`city_id`),
  CONSTRAINT `profile_city` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
  CONSTRAINT `profile_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы testingBD.profile: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` (`id`, `age`, `tel`, `city_id`, `user_id`, `gender`) VALUES
	(2, '24', '0994567633', 7, 27, 'женщина'),
	(4, '32', '0987773434', 6, 25, 'мужчина'),
	(8, '29', '0932223456', 8, 23, 'мужчина'),
	(9, '43', '0986745634', 8, 24, 'мужчина'),
	(12, '36', '0937872734', 6, 26, 'женщина');
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;

-- Дамп структуры для таблица testingBD.regions
CREATE TABLE IF NOT EXISTS `regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `region` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `region_country` (`country_id`),
  CONSTRAINT `region_country` FOREIGN KEY (`country_id`) REFERENCES `countryes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы testingBD.regions: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `regions` DISABLE KEYS */;
INSERT INTO `regions` (`id`, `country_id`, `region`) VALUES
	(5, 2, 'Днепропетровская область'),
	(6, 2, 'Киевская область'),
	(7, 2, 'Запорожская область');
/*!40000 ALTER TABLE `regions` ENABLE KEYS */;

-- Дамп структуры для таблица testingBD.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(90) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы testingBD.roles: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `role`) VALUES
	(11, 'менеджер'),
	(12, 'клиент'),
	(13, 'вебмастер');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Дамп структуры для таблица testingBD.status
CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы testingBD.status: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` (`id`, `status`) VALUES
	(1, 'Доставлено'),
	(2, 'В обработке'),
	(3, 'Отменен');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;

-- Дамп структуры для таблица testingBD.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(150) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role_id` int(20) NOT NULL,
  `realname` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `user_role` (`role_id`),
  CONSTRAINT `user_role` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы testingBD.users: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `email`, `password`, `date`, `role_id`, `realname`) VALUES
	(23, 'so@gmail.com', '12345', '2017-07-21 12:29:09', 11, 'Виктор'),
	(24, 'yu@gmail.com', '12345', '2017-07-21 12:29:42', 13, 'Юрий'),
	(25, 'ol@gmail.com', '12345', '2017-07-21 12:30:30', 12, 'Анатолий'),
	(26, 'yuli@gmail.com', '12345', '2017-07-21 12:30:50', 12, 'Юлия'),
	(27, 'nast@gmail.com', '12345', '2017-07-21 12:31:53', 12, 'Настя');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Дамп структуры для таблица testingBD.volume
CREATE TABLE IF NOT EXISTS `volume` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `volume` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы testingBD.volume: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `volume` DISABLE KEYS */;
INSERT INTO `volume` (`id`, `volume`) VALUES
	(10, '0.5'),
	(11, '0.7'),
	(12, '1'),
	(13, '1.5'),
	(14, '2');
/*!40000 ALTER TABLE `volume` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
testingbdBeerShopBeerShop