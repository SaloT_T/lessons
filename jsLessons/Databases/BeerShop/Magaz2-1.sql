SELECT u.real_name, u.email, b.name, p.price, o.date
FROM users u, orders o, products p, brands b
WHERE u.id = o.user_id 
AND o.product_id = p.id
AND p.brand_id = b.id
AND o.date BETWEEN '2017-07-22' AND '2017-07-24'