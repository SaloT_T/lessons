function BombKiller(n) {
    function randomize() {
        return Math.floor(Math.random() * (n - 1)) + 1;
    }
    this.start = function () {
        var code = randomize();
        console.log(code);
        var sector = document.getElementsByClassName('sector');
        sector[--code].nobomb = 1;
        console.log(sector[--code]);
        for (var i = 0; i < sector.length; i++) {
            console.log(code, i);
            sector[i].onclick = function () {
                var z = i;
                console.log(code, z);
                if (this.nobomb == 1) {
                    console.log(z);
                    this.style.backgroundColor = "green";
                }
                else {
                    console.log(z);
                    this.style.backgroundColor = "red";
                }
            }
        }
    }
}
var bomb = new BombKiller(4);
bomb.start();