Element.prototype.append = function(el){
    var newEl = document.createElement(el);
    this.appendChild(newEl);
    return newEl;
}
Element.prototype.appendText = function(el){
    var newEl = document.createTextNode(el);
    this.appendChild(newEl);
    return newEl;
}
Element.prototype.remove = function(){
    this.parentElement.removeChild(this);
}
var btn = document.getElementById('btn');

btn.onclick = function(){
    var prod = document.getElementById('newproduct').value;
    var list = document.getElementById('list');
    var newP = list.append('li');
    var ch = newP.append('input');
    ch.setAttribute('type', 'checkbox');
    var s = newP.append('span');
    s.setAttribute('class','text');
    s.appendText(prod);
    var r =  newP.append('i');
    r.setAttribute('class','fa fa-times');
    r.onclick = function(){
        this.parentElement.remove();
    }
    document.getElementById('newproduct').value="";
}